<!DOCTYPE html>
<html ng-app="mod1">
<head>
    <meta charset="UTF-8">
    <title>Esqueleto NG - CI</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- angular -->
    <script src="js/libraries/angular.min.js"></script>
    <script src="js/libraries/angular-route.min.js"></script>
    <!-- Nuestro modulo y controlador angular -->
    <script src="js/ng/mod1/mod1.js"></script>
    <script src="js/ng/mod1/controllers/mainController.js"></script>
    <script src="js/ng/mod1/controllers/homeController.js"></script>
    <script src="js/ng/mod1/controllers/aboutController.js"></script>
    <script src="js/ng/mod1/controllers/contactController.js"></script>
    <!-- jQuery 2.0.2 -->
    <script src="js/libraries/jquery-2.1.1.min.js"></script>
</head>
<!-- Aqui puede ir nuestro menu -->
<!-- HEADER AND NAVBAR -->
<header>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Angular Routing Example</a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><i class="glyphicon glyphicon-star-home"></i> Home</a></li>
                <li><a href="#about"><i class="glyphicon glyphicon-th-large"></i> About</a></li>
                <li><a href="#contact"><i class="glyphicon glyphicon-asterisk"></i> Contact</a></li>
            </ul>
        </div>
    </nav>
</header>
<!-- Definimos el controlador de angular -->
<body ng-controller="mainController">