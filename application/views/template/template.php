<!-- Encabezado de pagina -->
<?php $this->load->view('template/header'); ?>
<!-- Vista que sera el contenido -->
<?php $this->load->view($main_content);?>
<!-- Pie de pagina -->
<?php $this->load->view('template/footer');?>
<!-- Archivos php con codigo js -->
<?php if(isset($script)){ $this->load->view($script); } ?>