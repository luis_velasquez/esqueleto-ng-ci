<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Luis Velasquez
 * Description: Home controller class
 */
class Contact extends CI_Controller {

    function Contact() {
        parent::__construct();

    }

    function index() {
        //Cargando vista
        $this->load->view('contact_view');

    }//Fin index

}//Fin controlador