<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Luis Velasquez
 * Description: Home controller class
 */
class Inicio extends CI_Controller {

    function Inicio() {
        parent::__construct();

    }

    function index() {
        //Mandando por variable nombre de la vista al template
        $data['main_content'] = 'index_view';

        $this->load->view('template/template', $data);

    }//Fin index

}//Fin controlador