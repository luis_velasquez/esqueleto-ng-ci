<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Luis Velasquez
 * Description: Home controller class
 */
class Home extends CI_Controller {

    function Home() {
        parent::__construct();

    }

    function index() {
        //Cargando vista
        $this->load->view('home_view');

    }//Fin index

}//Fin controlador