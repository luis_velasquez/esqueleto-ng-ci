<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Luis Velasquez
 * Description: Home controller class
 */
class About extends CI_Controller {

    function About() {
        parent::__construct();

    }

    function index() {
        //Cargando vista
        $this->load->view('about_view');

    }//Fin index

}//Fin controlador