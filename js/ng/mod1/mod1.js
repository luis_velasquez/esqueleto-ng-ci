/**
 * Created by root on 09-28-14.
 */
// create the module and name it mod1
var mod1 = angular.module('mod1', ['ngRoute']);

// configurando nuestras rutas
mod1.config(function($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl : 'home',
            controller  : 'homeController'
        })

        // route for the about page
        .when('/about', {
            templateUrl : 'about',
            controller  : 'aboutController'
        })

        // route for the contact page
        .when('/contact', {
            templateUrl : 'contact',
            controller  : 'contactController'
        });
});